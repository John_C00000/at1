// Keep FX rates updated

var baseURL = "https://api.exchangeratesapi.io/latest?base=";

let fxTable = base.getTable("Live FOREX data");
let fxQuery = await fxTable.selectRecordsAsync();

let tmpfxTable = base.getTable("Live FOREX data");

var _currCode;

var updates = [];

for (let record of fxQuery.records) {
     
       _currCode = record.getCellValue("Code").toString();
    
        let apiResponse = await fetch(baseURL+_currCode);
        let jsonData = await apiResponse.json();

        for (var rate in jsonData.rates)   {
    
            updates= [];
            updates.push({
                id: record.id,
                fields: {
                       [rate] : jsonData.rates[rate]
                }
             }); 
            
             await tmpfxTable.updateRecordsAsync(updates);
        }
            
 
    }

}